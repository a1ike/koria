jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.open-card-1').on('click', function (e) {
    e.preventDefault();

    $('.k-modal_1').toggle();
    $('.k-modal_1').find('.k-tabs__header li:first-child').click();
    swiper.update();
    swiper2.update();
  });

  $('.open-card-2').on('click', function (e) {
    e.preventDefault();

    $('.k-modal_2').toggle();
    $('.k-modal_2').find('.k-tabs__header li:first-child').click();
    s2wiper.update();
    s2wiper2.update();
  });

  $('.open-card-3').on('click', function (e) {
    e.preventDefault();

    $('.k-modal_3').toggle();
    $('.k-modal_3').find('.k-tabs__header li:first-child').click();
    s3wiper.update();
    s3wiper2.update();
  });

  $('.open-card-4').on('click', function (e) {
    e.preventDefault();

    $('.k-modal_4').toggle();
    $('.k-modal_4').find('.k-tabs__header li:first-child').click();
    s4wiper.update();
    s4wiper2.update();
  });

  $('.k-modal__centered').on('click', function (e) {
    if (e.target.className === 'k-modal__centered') {
      $('.k-modal').hide();
    }
  });

  $('.k-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.k-modal').hide();
  });

  $('.k-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.k-tabs__header li').removeClass('current');
    $('.k-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  new Swiper('.k-features__cards', {
    navigation: {
      nextEl: '.k-features__cards .swiper-button-next',
      prevEl: '.k-features__cards .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
  });

  new Swiper('.k-work__cards', {
    pagination: {
      el: '.k-work__cards .swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.k-work__cards .swiper-button-next',
      prevEl: '.k-work__cards .swiper-button-prev',
    },
    loop: false,
    spaceBetween: 30,
  });

  new Swiper('.k-goal__cards', {
    navigation: {
      nextEl: '.k-goal .swiper-button-next',
      prevEl: '.k-goal .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 10,
    slidesPerView: 1,
    breakpoints: {
      760: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
    },
  });

  var swiper = new Swiper('.mySwiper', {
    spaceBetween: 15,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var swiper2 = new Swiper('.mySwiper2', {
    spaceBetween: 15,
    navigation: {
      nextEl: '.k-modal .swiper-button-next',
      prevEl: '.k-modal .swiper-button-prev',
    },
    thumbs: {
      swiper: swiper,
    },
  });

  var s2wiper = new Swiper('.m2ySwiper', {
    spaceBetween: 15,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var s2wiper2 = new Swiper('.m2ySwiper2', {
    spaceBetween: 15,
    navigation: {
      nextEl: '.k-modal_2 .swiper-button-next',
      prevEl: '.k-modal_2 .swiper-button-prev',
    },
    thumbs: {
      swiper: s2wiper,
    },
  });

  var s3wiper = new Swiper('.m3ySwiper', {
    spaceBetween: 15,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var s3wiper2 = new Swiper('.m3ySwiper2', {
    spaceBetween: 15,
    navigation: {
      nextEl: '.k-modal_3 .swiper-button-next',
      prevEl: '.k-modal_3 .swiper-button-prev',
    },
    thumbs: {
      swiper: s3wiper,
    },
  });

  var s4wiper = new Swiper('.m4ySwiper', {
    spaceBetween: 15,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var s4wiper2 = new Swiper('.m4ySwiper2', {
    spaceBetween: 15,
    navigation: {
      nextEl: '.k-modal_4 .swiper-button-next',
      prevEl: '.k-modal_4 .swiper-button-prev',
    },
    thumbs: {
      swiper: s4wiper,
    },
  });
});
